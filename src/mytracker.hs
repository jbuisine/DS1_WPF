{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

import qualified Clay as C
import Control.Monad.Trans (liftIO)
import Data.Aeson (ToJSON)
import qualified Data.Text as T
import qualified Data.Text.Lazy as L
import qualified Database.SQLite.Simple as SQL
import Database.SQLite.Simple.FromRow (FromRow, fromRow, field)
import GHC.Generics (Generic)
import Lucid
import Network.Wai.Middleware.Cors (simpleCors)
import Network.Wai.Middleware.RequestLogger (logStdoutDev)
import Web.Scotty (middleware, scotty, get, json, html, param, ActionM)

-- PARAMS
dbName :: String
dbName = "mytracker.db"

-- DATA Part
data Tracker = Tracker 
  { site :: T.Text
  , page :: T.Text
  , hits :: Int
  } deriving (Show, Generic)

instance FromRow Tracker where
  fromRow = Tracker <$> field <*> field <*> field

instance ToJSON Tracker

-- QUERIES PART
-- selectAll function
selectAll :: IO [Tracker]
selectAll = do
  conn <- SQL.open dbName
  let sql = "SELECT * FROM tracker" 
  trackers <- SQL.query_ conn sql :: IO [Tracker]
  SQL.close conn
  return trackers

 
-- select specific page
selectOne :: T.Text -> T.Text -> IO [Tracker]
selectOne site page = do
  conn <- SQL.open dbName
  let sql = "SELECT * FROM tracker where site = ? and page = ?" 
  trackers <- SQL.query conn sql (site::T.Text, page::T.Text) :: IO [Tracker]
  SQL.close conn
  return trackers

updateHits :: T.Text -> T.Text -> IO ()
updateHits site page = do
  conn <- SQL.open dbName
  let sql = "UPDATE tracker SET hits = hits + 1 WHERE site = ? and page = ?" 
  SQL.execute conn sql (site::T.Text, page::T.Text)
  SQL.close conn


-- ROUTES PART
main :: IO ()
main = scotty 3000 $ do
  middleware logStdoutDev
  middleware simpleCors

  -- Define home route
  get "/" $ do
    -- Getting array of trackers from database
    -- escape IO data type
    trackers <- liftIO selectAll
    html $ mkpage "Home - Trackers Information" $ homeRoute trackers
  
  -- json export route
  get "/json_export" $ do 
    trackers <- liftIO selectAll
    json $ trackers
  
  get "/tracker" $ do
    site <- param "site"
    page <- param "page"
    trackers <- liftIO (selectOne site page)
    liftIO (updateHits site page)
    renderTracker (T.pack (show (hits (head trackers))))


-- RENDERING PART
-- to improve CSS (not class but based on elements)
tableCss :: C.Css
tableCss = C.table C.# C.byClass "tableCss" C.? do
  C.border           C.solid (C.px 1) C.black

tdCss :: C.Css
tdCss = C.td C.# C.byClass "tdCss" C.? do
  C.border           C.solid (C.px 1) C.black

mkpage :: Lucid.Html () -> Lucid.Html () -> L.Text
mkpage titleStr page = renderText $ do
  doctype_
  html_ $ do
    head_ $ do
      style_ $ L.toStrict $ C.render tableCss
      style_ $ L.toStrict $ C.render tdCss
    header_ $ do
      title_ titleStr
    body_ page

homeRoute :: [Tracker] -> Lucid.Html ()
homeRoute trackers = do
  h1_ "mytracker"
  table_ [class_ "tableCss"] $ (mapM_ renderTrackList trackers)
  a_ [href_ "json_export"] "get json"


-- UTILS PART
-- Part which contains utils rendering function
renderTracker :: T.Text -> ActionM ()
renderTracker msg = html $ renderText $ do
  doctype_
  html_ $ do
    header_ $ do
      title_ "Generated page"
    body_ $ do
      toHtml msg
      a_ [href_ "/"] "Go home"

renderTrackList :: Tracker -> Lucid.Html()
renderTrackList tracker = do
  tr_ $ do
    -- TODO : improve CSS use...
    td_ [class_ "tdCss" ] (toHtml (site tracker))
    td_ [class_ "tdCss" ] $ do
       a_ [href_ link] (toHtml (page tracker))
    td_ [class_ "tdCss" ] (toHtml (show (hits tracker)))
  -- Update link to avoid \" quote
  where link = T.concat ["/tracker?site=", site tracker, "&page=", T.pack (show (page tracker))]
